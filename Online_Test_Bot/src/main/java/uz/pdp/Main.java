package uz.pdp;

import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import uz.pdp.bot.*;
import uz.pdp.db.*;
import uz.pdp.model.*;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws TelegramApiException {

        TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
        telegramBotsApi.registerBot(new DtmBot());
        User admin = new User("Boburbek", "+998993960661", "1348585856", BotState.SHARE_CONTACT, "0661");
        DB.userList.add(admin);


        // From English
        Subject_ subject = new Subject_("English", 15, 5, 3);
        DB.subjectList.add(subject);

        // test 1:
        List<Answer> answerOfTest1 = new ArrayList<>();
        Answer t_1Ans1 = new Answer("the richest", true);
        Answer t_1Ans2 = new Answer("the most rich", false);
        Answer t_1Ans3 = new Answer("the rich", false);
        answerOfTest1.add(t_1Ans1);
        answerOfTest1.add(t_1Ans2);
        answerOfTest1.add(t_1Ans3);
        Test test1 = new Test("1. Who is ... person in the world?", subject, answerOfTest1);


        // test 2:
        List<Answer> answerOfTest2 = new ArrayList<>();
        Answer t_2Ans1 = new Answer("hard", false);
        Answer t_2Ans2 = new Answer("the most hard", false);
        Answer t_2Ans3 = new Answer("harder", true);
        answerOfTest2.add(t_2Ans1);
        answerOfTest2.add(t_2Ans2);
        answerOfTest2.add(t_2Ans3);
        Test test2 = new Test("2. This is ... than I expected!", subject, answerOfTest2);


        // test 3:
        List<Answer> answerOfTest3 = new ArrayList<>();
        Answer t_3Ans1 = new Answer("is snowing", false);
        Answer t_3Ans2 = new Answer("snows", true);
        Answer t_3Ans3 = new Answer("snowed", false);
        answerOfTest3.add(t_3Ans1);
        answerOfTest3.add(t_3Ans2);
        answerOfTest3.add(t_3Ans3);
        Test test3 = new Test("3. It ... many times every winter in Frankfurt.", subject, answerOfTest3);

        DB.testList.add(test1);
        DB.testList.add(test2);
        DB.testList.add(test3);

        // From math
        Subject_ math = new Subject_("Math", 10, 5, 2);
        DB.subjectList.add(math);

        // test 1:
        List<Answer> answerListOfTest1 = new ArrayList<>();
        Answer test1_ans1 = new Answer("2220", false);
        Answer test1_ans2 = new Answer("2160", true);
        Answer test1_ans3 = new Answer("2100", false);
        answerListOfTest1.add(test1_ans1);
        answerListOfTest1.add(test1_ans2);
        answerListOfTest1.add(test1_ans3);
        Test test1_Math = new Test("1. Hozir soat 19:24. Soat 20:00 bo'lishi uchun necha sekund o'tishi kerak?", math, answerListOfTest1);

        // test 2:
        List<Answer> answerListOfTest2 = new ArrayList<>();
        Answer test2_ans1 = new Answer("4²+5²", false);
        Answer test2_ans2 = new Answer("4²+2•4•5+5²", true);
        Answer test2_ans3 = new Answer("4•2+5•2", false);
        answerListOfTest2.add(test2_ans1);
        answerListOfTest2.add(test2_ans2);
        answerListOfTest2.add(test2_ans3);
        Test test2_Math = new Test("2. (4+5)² quyidagilardan qaysi biriga teng?", math, answerListOfTest2);

        DB.testList.add(test1_Math);
        DB.testList.add(test2_Math);
    }
}
