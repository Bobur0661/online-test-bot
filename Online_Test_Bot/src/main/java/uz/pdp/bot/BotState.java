package uz.pdp.bot;

public interface BotState {

    String START = "START";
    String SHARE_CONTACT = "SHARE_CONTACT";
    String SHOW_MAIN_USER_MENU = "SHOW_MAIN_USER_MENU";
    String DOWNLOADING = "DOWNLOADING";
    String CHOOSE_SUBJECT = "CHOOSE_SUBJECT";
    String DO_TEST= "DO_TEST";
    String CHECK_ANSWER= "CHECK_ANSWER";
}
