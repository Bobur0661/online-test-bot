package uz.pdp.bot;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import lombok.SneakyThrows;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import uz.pdp.db.*;
import uz.pdp.model.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class BotService {

    public static String finalInfo = "";

    public static SendMessage start(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText("\n \uD83D\uDCD8 DTM bot ga Xush kelibsiz! \uD83D\uDCD9\n \n \uD83D\uDCF1 Kontaktingizni jo'nating \uD83D\uDCF1  ");

        String chatId = getChatId(update);
        User user = getOrCreateTgUser(chatId);

        user.setState(BotState.START);
        BotService.saveUserChanges(user);

        if (user.getState().equals(BotState.START)) {
            user.setState(BotState.SHARE_CONTACT);
            BotService.saveUserChanges(user);

            ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
            List<KeyboardRow> rowList = new ArrayList<>();
            KeyboardRow row1 = new KeyboardRow();
            KeyboardButton row1Button1 = new KeyboardButton();
            row1Button1.setText("\uD83D\uDCDE Jo'natish \uD83D\uDCDE");
            row1Button1.setRequestContact(true);
            row1.add(row1Button1);
            rowList.add(row1);

            markup.setKeyboard(rowList);
            markup.setSelective(true);
            markup.setResizeKeyboard(true);
            sendMessage.setChatId(chatId);
            sendMessage.setReplyMarkup(markup);
            return sendMessage;
        } else {
            return showUserMainMenu(user, update);
        }
    }


    public static SendMessage showUserMainMenu(User user, Update update) {
        SendMessage sendMessage = new SendMessage();
        String chatId = getChatId(update);
        if (user.getState().equals(BotState.SHARE_CONTACT) || user.getState().equals(BotState.SHOW_MAIN_USER_MENU)) {
            user.setState(BotState.SHOW_MAIN_USER_MENU);
            saveUserChanges(user);
            InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
            List<InlineKeyboardButton> row1 = new ArrayList<>();
            List<InlineKeyboardButton> row2 = new ArrayList<>();
            InlineKeyboardButton row1Button1 = new InlineKeyboardButton();
            InlineKeyboardButton row2Button1 = new InlineKeyboardButton();
            row1Button1.setText(" \uD83D\uDD0E Test ishlash \uD83D\uDD0D");
            row2Button1.setText(" \uD83D\uDDC2 Ishlangan testlarni yuklab olish (pdf) \uD83D\uDDC2");
            row1Button1.setCallbackData("SOLVE_TEST");
            row2Button1.setCallbackData("DOWNLOAD");
            row1.add(row1Button1);
            row2.add(row2Button1);
            rowList.add(row1);
            rowList.add(row2);

            markup.setKeyboard(rowList);
            sendMessage.setChatId(chatId);
            sendMessage.setReplyMarkup(markup);
        }
        sendMessage.setText("\uD83D\uDCCC Menu tanlang \uD83D\uDCCC ");
        return sendMessage;
    }


    public static SendMessage chooseSubject(Update update) {
        SendMessage sendMessage = new SendMessage();
        String chatId = getChatId(update);
        User user = getOrCreateTgUser(chatId);
        user.setState(BotState.CHOOSE_SUBJECT);
        saveUserChanges(user);
        sendMessage.setChatId(chatId);
        sendMessage.setText("  ✏️  Test ishlamoqchi bo'lgan fanni tanlang:   ✏️  ");
        sendMessage.setReplyMarkup(generateInlineKeyboardMarkup(user));
        return sendMessage;
    }


    public static InlineKeyboardMarkup generateInlineKeyboardMarkup(User user) {
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton row1Button1 = new InlineKeyboardButton();
        if (user.getState().equals(BotState.CHOOSE_SUBJECT)) {
            for (int i = 0; i < DB.subjectList.size(); i++) {
                if (i % 3 == 0) {
                    rowList.add(row1);
                    row1 = new ArrayList<>();
                }
                row1Button1 = new InlineKeyboardButton();
                row1Button1.setText(DB.subjectList.get(i).getSubjectName());
                row1Button1.setCallbackData("Sub" + DB.subjectList.get(i).getSubjectName());
                row1.add(row1Button1);
            }

            List<InlineKeyboardButton> row2 = new ArrayList<>();
            InlineKeyboardButton row2Button1 = new InlineKeyboardButton();
            row2Button1.setText("\uD83D\uDD19 Back \uD83D\uDD19");
            row2Button1.setCallbackData("Back to Main");
            row2.add(row2Button1);
            rowList.add(row1);
            rowList.add(row2);
        }
        markup.setKeyboard(rowList);
        return markup;
    }

    public static SendMessage testForm(Test test, String chatId) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(test.getQuestion());

        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button = new InlineKeyboardButton();

        for (int i = 0; i < test.getAnswers().size(); i++) {
            button.setText(test.getAnswers().get(i).getAnswerName());
            button.setCallbackData(test.getAnswers().get(i).getAnswerName());
            row1.add(button);
            rowList.add(row1);
            button = new InlineKeyboardButton();
            row1 = new ArrayList<>();
        }
        markup.setKeyboard(rowList);
        sendMessage.setReplyMarkup(markup);
        return sendMessage;
    }

    public static SendMessage doTest(User tgUser, String chatId) {
        SendMessage sendMessage = new SendMessage();
        for (int i = tgUser.getCurrentTestNumber(); i < tgUser.getCurrentTests().size(); i++) {
            tgUser.setCurrentTest(tgUser.getCurrentTests().get(i));
            saveUserChanges(tgUser);
            return testForm(tgUser.getCurrentTests().get(i), chatId);
        }
        if (tgUser.getCurrentTestNumber() == tgUser.getCurrentTests().size()) {
            UserSolvedTestHistory userSolvedTestHistory = new UserSolvedTestHistory(tgUser, tgUser.getCurrentBall(), tgUser.getCurrentTest().getSubject());
            DB.historyList.add(userSolvedTestHistory);
            sendMessage.setChatId(chatId);
            finalInfo = "Exam from: " + tgUser.getCurrentTest().getSubject().getSubjectName() + "\n" +
                    "Maximum score in English: " + tgUser.getCurrentTest().getSubject().getMaxScore() + "\n" +
                    "Number of tests: " + tgUser.getCurrentTest().getSubject().getNumberOfTest() + "\n" +
                    "Score for each right answer: " + tgUser.getCurrentTest().getSubject().getScoreForEachTest() + "\n" +
                    "Your total ball: " + tgUser.getCurrentBall();

            sendMessage.setText(finalInfo);

            tgUser.setCurrentTest(null);
            tgUser.setCurrentBall(0);
            tgUser.setCurrentTests(null);
            tgUser.setCurrentTestNumber(0);

            InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
            List<InlineKeyboardButton> row1 = new ArrayList<>();
            InlineKeyboardButton row1Button1 = new InlineKeyboardButton();
            row1Button1.setText("◀️Back ◀️");
            row1Button1.setCallbackData("Back_");
            row1.add(row1Button1);
            rowList.add(row1);
            markup.setKeyboard(rowList);
            sendMessage.setReplyMarkup(markup);
            return sendMessage;
        }
        return sendMessage;
    }

    // Bu method bir sabject ga tegishli bo'lgan hamma testlarni olvoladi.
    public static List<Test> subjectTests(String subjectName) {
        List<Test> testList = new ArrayList<>();
        for (Test test : DB.testList) {
            if (test.getSubject().getSubjectName().equals(subjectName)) {
                testList.add(test);
            }
        }
        return testList;
    }

//   public static File testSolveHistory(UserSolvedTestHistory userSolvedTestHistory){
//
//   }

    public static void saveUserChanges(User changedUser) {
        for (User user : DB.userList) {
            if (user.getChatId().equals(changedUser.getChatId())) {
                user = changedUser;
            }
        }
    }

    public static User getOrCreateTgUser(String chatId) {
        for (User user : DB.userList) {
            if (user.getChatId().equals(chatId)) {
                return user;
            }
        }
        User user = new User(chatId, BotState.START);
        DB.userList.add(user);
        return user;
    }

    public static String getChatId(Update update) {
        if (update.hasMessage()) {
            return update.getMessage().getChatId().toString();
        } else if (update.hasCallbackQuery()) {
            return update.getCallbackQuery().getMessage().getChatId().toString();
        }
        return "";
    }


    @SneakyThrows
    public static SendDocument pdfSendMethod(User user) {
        user.setState(BotState.DOWNLOADING);

        SendDocument sendDocument = new SendDocument();
        sendDocument.setChatId(user.getChatId());

        File file = new File("src/main/resources/userScore.pdf");
        PdfWriter writer = new PdfWriter(file);
        PdfDocument pdfDoc = new PdfDocument(writer);
        pdfDoc.addNewPage();
        Document document = new Document(pdfDoc);

        Paragraph paragraph = new Paragraph("A list of tests that the user has solved: ");
        paragraph.setTextAlignment(TextAlignment.CENTER);
        document.add(paragraph);
        float [] column = {100f, 100f, 100f, 100f, 100f};
//        Exam from: English
//        Maximum score in English: 15.0
//        Number of tests: 3
//        Score for each right answer: 5.0
//        Your total ball: 10.0
        Table table = new Table(column);
        table.addCell("Exam from:");
        table.addCell("Maximum score:");
        table.addCell("Number of tests:");
        table.addCell("Score for each correct answer:");
        table.addCell("Your total score:");
        for (UserSolvedTestHistory history : DB.historyList) {
            if (history.getUser().equals(user)){
                table.addCell(history.getSubject().getSubjectName()).setTextAlignment(TextAlignment.CENTER);
                table.addCell(String.valueOf(history.getSubject().getMaxScore())).setTextAlignment(TextAlignment.CENTER);
                table.addCell(String.valueOf(history.getSubject().getNumberOfTest())).setTextAlignment(TextAlignment.CENTER);
                table.addCell(String.valueOf(history.getSubject().getScoreForEachTest())).setTextAlignment(TextAlignment.CENTER);
                table.addCell(String.valueOf(history.getTotalBall())).setTextAlignment(TextAlignment.CENTER);
            }
        }
        document.add(table);
        document.close();

        sendDocument.setCaption("\uD83D\uDCC8 User's test Score \uD83D\uDCC8");
        InputFile inputFile = new InputFile(file);
        sendDocument.setDocument(inputFile);
        saveUserChanges(user);
        return sendDocument;
    }
}
