package uz.pdp.bot;

import lombok.SneakyThrows;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Contact;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import uz.pdp.model.Answer;
import uz.pdp.model.Test;
import uz.pdp.model.User;

import java.util.List;

import static uz.pdp.bot.BotService.*;


public class DtmBot extends TelegramLongPollingBot {

    @Override
    public String getBotUsername() {
        return "Dtm_BotV2_B5_bot";
    }

    @Override
    public String getBotToken() {
        return "5093253963:AAGTfuYwcCK9tU9NvLb6KGRwon689EjwMrU";
    }

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {
        User user = getOrCreateTgUser(getChatId(update));
        String chatId = getChatId(update);
        if (update.hasMessage()) {
            if (update.getMessage().hasText()) {
                String text = update.getMessage().getText();
                if (text.equals("/start")) {
                    if (user.getState().equals(BotState.SHOW_MAIN_USER_MENU)) {
                        deleteMessage(user);
                        SendMessage message = showUserMainMenu(user, update);
                        execute(message);
                    } else {
                        SendMessage message = start(update);
                        execute(message);
                    }
                }
            } else if (update.getMessage().hasContact()) {
                Contact contact = update.getMessage().getContact();
                if (contact.getPhoneNumber().equals("+998993960661")) {
                    // TODO Admin menu:
                } else {
                    deleteMessage(user);
                    SendMessage message = showUserMainMenu(user, update);
                    execute(message);
                }
            }
        }
        else if (update.hasCallbackQuery()) {
            User userInCallBakMenu = getOrCreateTgUser(getChatId(update));
            String data = update.getCallbackQuery().getData();
            if (data.equals("SOLVE_TEST")) {
                execute(chooseSubject(update));
            } else if (data.equals("Back to Main")) {
                user.setState(BotState.SHOW_MAIN_USER_MENU);
                saveUserChanges(user);
                SendMessage message = showUserMainMenu(user, update);
                execute(message);
            } else if (data.equals("Back_")) {
                user.setState(BotState.CHOOSE_SUBJECT);
                saveUserChanges(user);
                SendMessage message = chooseSubject(update);
                execute(message);
            }
            else if (user.getState().equals(BotState.CHOOSE_SUBJECT)) {
                if (data.startsWith("Sub")) {
                    user.setState(BotState.CHECK_ANSWER);
                    List<Test> testList = subjectTests(data.substring(3));
                    user.setCurrentTests(testList);
                    user.setCurrentTest(testList.get(0));
                    saveUserChanges(user);
                    execute(testForm(testList.get(0), chatId));
                }
            } else if (user.getState().equals(BotState.CHECK_ANSWER)) {
                boolean isTrue = false;
                for (Answer answer : user.getCurrentTest().getAnswers()) {
                    if (answer.getAnswerName().equals(data) && answer.isCorrect()) {
                        isTrue = true;
                        break;
                    } else if (answer.getAnswerName().equals(data) && !answer.isCorrect()) {
                        isTrue = false;
                        break;
                    }
                }
                if (isTrue) {
                    user.setCurrentBall(user.getCurrentBall() + user.getCurrentTest().getSubject().getScoreForEachTest());
                }
                user.setCurrentTestNumber(user.getCurrentTestNumber() + 1);
                execute(doTest(user, chatId));
            } else if (data.equals("DOWNLOAD")) {
                execute(pdfSendMethod(user));
            }
        }
    }

    @SneakyThrows
    public void deleteMessage(User user) {
        SendMessage sendMessageRemove = new SendMessage();
        sendMessageRemove.setChatId(user.getChatId());
        sendMessageRemove.setText("\uD83C\uDF1F");
        sendMessageRemove.setReplyMarkup(new ReplyKeyboardRemove(true));
        Message message = execute(sendMessageRemove);
        DeleteMessage deleteMessage = new DeleteMessage(user.getChatId(), message.getMessageId());
        execute(deleteMessage);
    }

}
