package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Test {

    private UUID id = UUID.randomUUID();
    private String  question;
    private Subject_ subject;
    List<Answer> answers = new ArrayList<>();

    public Test(String question, Subject_ subject, List<Answer> answers) {
        this.question = question;
        this.subject  = subject;
        this.answers  = answers;
    }
}
