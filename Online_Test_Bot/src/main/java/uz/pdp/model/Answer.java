package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Answer {

    private UUID id = UUID.randomUUID();
    private String answerName;
    private boolean correct;

    public Answer(String answerName, boolean correct) {
        this.answerName = answerName;
        this.correct = correct;
    }
}
