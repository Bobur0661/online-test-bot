package uz.pdp.model;

import uz.pdp.model.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSolvedTestHistory {

    private UUID id = UUID.randomUUID();
    private User user;
    private double totalBall;
    private Subject_ subject;

    public UserSolvedTestHistory(User user, double totalBall, Subject_ subject) {
        this.user = user;
        this.totalBall = totalBall;
        this.subject = subject;
    }
}
