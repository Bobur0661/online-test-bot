package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private UUID id;
    private String username;
    private String phoneNumber;
    private String chatId;
    private String state;
    private String password;
    private Test currentTest;
    private double currentBall;
    private int currentTestNumber;
    private List<Test> currentTests = new ArrayList<>();
    public User(String username, String phoneNumber, String chatId, String state, String password) {
        this.username = username;
        this.phoneNumber = phoneNumber;
        this.chatId = chatId;
        this.state = state;
        this.password = password;
    }


    public User(String chatId, String state) {
        this.chatId = chatId;
        this.state = state;
    }
}
